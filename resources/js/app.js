
import axios from 'axios';
require('./bootstrap');

window.Vue = require('vue');

Vue.component('my-component', require('./components/MyComponent.vue').default);
Vue.component('form-component', require('./components/FormComponent.vue').default);
Vue.component('usuario-component', require('./components/UsuarioComponent.vue').default);
Vue.prototype.$http = axios;

const app = new Vue({
    el: '#app',
});
